import javax.swing.JOptionPane;

public class Jogo1 {

	Imagem imagem;
	Player player = new Player(1, "Player", false);
	Computador computador = new Computador(2, "Computador", false);
	TelaPrincipal tabuleiro;
	
	int li = 0, co = 0;
	boolean acertou;
	
	public void vez() {

		acertou = false;

		if(player.getVez()) {

			computador.setVez(true);
			player.setVez(false);
			atiraComputador();
		}

		else {
			computador.setVez(false);
			player.setVez(true);
			atiraPlayer();
		}
	}

	public boolean atira(Botao [][] tab, int l, int c, Jogador jogador) {

		if(tab[l][c].getId() >= 0) {

			if(tab[l][c].getId() == 0) {

				tab[l][c].setId(-1);
				tab[l][c].setIcon(imagem.agua);
			}

			if(tab[l][c].getId() > 0) {

				tab[l][c].setIcon(imagem.fogo);

				tab[l][c].setId(-1);

				if(jogador.getNum() == 1) { player.pontua(); }
				if(jogador.getNum() == 2) { computador.pontua(); }

				return true;
			}
		}

		if(player.getPontuacao() == 17) {

			JOptionPane.showMessageDialog(null,
				"Voc� ganhou!!!");

			System.exit(0);
		}

		else if(computador.getPontuacao() == 17) {

			JOptionPane.showMessageDialog(null,
				"Voc� perdeu...");

			System.exit(0);
		}

		return false;
	}

	public void atiraPlayer() {

		String tiro = "";
		int l = 0, c = 0;

		do {

			tiro = JOptionPane.showInputDialog(null,
				"Digite a linha e a coluna onde quer atirar "
				+ "\nda mesma forma que digitou para posicionar "
				+ "\nos navios.");

			l = Integer.parseInt(tiro.substring(0, 1));
			c = Integer.parseInt(tiro.substring(1, 2));
		}
		while(tiro.equals(""));

		acertou = atira(computador.tabuleiroComputador, l, c, player);

		if(acertou) {

			String nv = computador.tabuleiroComputador[l][c].getNome();

			JOptionPane.showMessageDialog(null,
				"Voc� acertou em um peda�o do " + nv + " do seu advers�rio!");
		}

		vez();
	}
	
	int l = 0, c = 0;

	public void atiraComputador() {

		if(li > 0) {
			if(li == 9)
				acertou = atira(player.tabuleiroPlayer, li, (co - 1), computador);
			else
				acertou = atira(player.tabuleiroPlayer, li, (co + 1), computador);
		}
		else {

			l = computador.sorteia(10);
			c = computador.sorteia(10);

			acertou = atira(player.tabuleiroPlayer, l, c, computador);
		}

		if(acertou) { li = l; co = c; }

		vez();
	}
}
