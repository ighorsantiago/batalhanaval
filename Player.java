import javax.swing.JOptionPane;

public class Player extends Jogador {
	
	public static final int HORIZONTAL = 0;
	public static final int VERTICAL = 1;
	
	Navio navio = new Navio();
	
	Botao [][] tabuleiroPlayer = new Botao [10][10];

	int navios = 1;
	
	public Player(int num, String nome, boolean vez) {
		
		super(1, nome, false);
	}
	
	public Botao getPosicao(int l, int c) { return tabuleiroPlayer[l][c]; }
	public void setPosicao(Botao [][] t, int l, int c) { tabuleiroPlayer[l][c] = t[l][c]; }

	public void posicionaPlayer(Botao btn) {

		int orientacao = 0, linha = 0, coluna = 0;
		
		if(navios <= 5) {
			
			navio = navio.constroiNavio(navios);
			
			JOptionPane.showMessageDialog(
					null, "Posicione o " + navio.getNome() + "."
						+ "\nEsse navio ocupa " + navio.getTamanho() + " espa�os."
						+ "\nEscolha o primeiro espa�o que ele ir� ocupar...");
		
			Object[] opcoes = {"Horizontal", "Vertical"};
			
			orientacao = JOptionPane.showOptionDialog(null,
								"Escolha a orienta��o do navio",
								"Escolha",
								JOptionPane.YES_NO_CANCEL_OPTION,
								JOptionPane.QUESTION_MESSAGE,
								null,
								opcoes,
								opcoes[1]);
				
			linha = btn.getLinha();
			coluna = btn.getColuna();
			
			if(navio.getTamanho() != 0) {

				if(orientacao < 2) {
					
					if(linha < 10) {
					
						if(coluna < 10) {
							
							if(navio.posicionaNavio("C", orientacao, linha, coluna, navio, tabuleiroPlayer)) {

								navios++;
							}
						}
					}
				}
			}
		}
		
		navio.posicionaNavio("P", orientacao, linha, coluna, navio, tabuleiroPlayer);
	}
}