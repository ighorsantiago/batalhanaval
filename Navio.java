public class Navio {

	public static final int BARCO_PATRULHA = 1;
	public static final int DESTROIER = 2;
	public static final int SUBMARINO = 3;
	public static final int ENCOURACADO = 4;
	public static final int PORTA_AVIOES = 5;
	public static final int HORIZONTAL = 0;
	public static final int VERTICAL = 1;
	
	String nome;
	int tamanho;
	int id;
	Jogador jogador;
	Ponto [] pontos = new Ponto [tamanho];
	Imagem imagem = new Imagem();
	Botao btn;
	
	public Navio(String nome, int tamanho, int identificador) {

		this.nome = nome;
		this.tamanho = tamanho;
		this.id = identificador;
	}

	public Navio() {}

	public String getNome() { return nome; }

	public int getTamanho() { return tamanho; }

	public int getId() { return id; }

	public Ponto getPonto(int indice) { return pontos[indice]; }
	public void setPonto(Ponto ponto, int posicao) { pontos[posicao] = ponto; }
	
	public Navio constroiNavio(int id) {

		if(id == BARCO_PATRULHA) {

			return new Navio("Barco de patrulha", 2, 1);
		}

		else if(id == DESTROIER) {

			return new Navio("Destr�ier", 3, 2);
		}

		else if(id == SUBMARINO) {

			return new Navio("Submarino", 3, 3);
		}

		else if(id == ENCOURACADO) {

			return new Navio("Encoura�ado", 4, 4);
		}

		else if(id == PORTA_AVIOES) {

			return new Navio("Porta-avi�es", 5, 5);
		}
		else
			return null;
	}
	
	public boolean verifica(int orientacao, int linha, int coluna, Navio navio, Botao [][] tabuleiro) {
		
		int cabe;
		
		if(orientacao == 0) {
			
			cabe = 0;
			
			if((coluna + navio.getTamanho()) <= 10) {
				
				for(int i = 0; i < navio.getTamanho(); i++) {
					
					if((tabuleiro[linha][coluna + i].getEstado()) == Estado.VAZIO) {
						
						cabe++;
					}
				}
				
				if(cabe == navio.getTamanho()) { return true; }
			}
		}
		
		if(orientacao == 1) {
			
			cabe = 0;
			
			if((linha + navio.getTamanho()) <= 10) {
				
				for(int i = 0; i < navio.getTamanho(); i++) {
					
					if((tabuleiro[linha + i][coluna].getEstado()) == Estado.VAZIO) {
						
						cabe++;
					}
				}
				
				if(cabe == navio.getTamanho()) { return true; }
			}
		}
		
		return false;
	}
	
	public boolean posicionaNavio(String jogador, int orientacao, int linha, int coluna, Navio navio, Botao [][] tabuleiro) {
		
		if(verifica(orientacao, linha, coluna, navio, tabuleiro)) {
					
			if(navio.getId() == BARCO_PATRULHA)
				desenhaBarcoPatrulha(orientacao, linha, coluna, tabuleiro);
			
			if(navio.getId() == DESTROIER)
				desenhaDestroier(orientacao, linha, coluna, tabuleiro);
			
			if(navio.getId() == SUBMARINO)
				desenhaSubmarino(orientacao, linha, coluna, tabuleiro);
			
			if(navio.getId() == ENCOURACADO)
				desenhaEncouracado(orientacao, linha, coluna, tabuleiro);
			
			if(navio.getId() == PORTA_AVIOES)
				desenhaPortaAvioes(orientacao, linha, coluna, tabuleiro);
			
			return true;
		}
		else
			return false;
	}

	public void desenhaBarcoPatrulha(int orientacao, int linha, int coluna, Botao [][] tabuleiro) {
		
		if(orientacao == HORIZONTAL) {
			
			for(int i = 0; i < 2; i++) {
				
				tabuleiro[linha][coluna + i].setId(1);
				tabuleiro[linha][coluna + i].setToolTipText("BARCO_PATRULHA");
				tabuleiro[linha][coluna + i].setEstado(Estado.NAVIO);
				tabuleiro[linha][coluna + i].setIcon(imagem.barcoPatrulha[i][orientacao]);
			}
		}
		
		if(orientacao == VERTICAL) {
			
			for(int i = 0; i < 2; i++) {
				
				tabuleiro[linha + i][coluna].setId(1);
				tabuleiro[linha + i][coluna].setToolTipText("BARCO_PATRULHA");
				tabuleiro[linha + i][coluna].setEstado(Estado.NAVIO);
				tabuleiro[linha + i][coluna].setIcon(imagem.barcoPatrulha[i][orientacao]);
			}
		}
	}
	
	public void desenhaDestroier(int orientacao, int linha, int coluna, Botao [][] tabuleiro) {
		
		if(orientacao == HORIZONTAL) {
			
			for(int i = 0; i < 3; i++) {
				
				tabuleiro[linha][coluna + i].setId(2);
				tabuleiro[linha][coluna + i].setToolTipText("DESTROIER");
				tabuleiro[linha][coluna + i].setEstado(Estado.NAVIO);
				tabuleiro[linha][coluna + i].setIcon(imagem.destroier[i][orientacao]);
			}
		}
		
		if(orientacao == VERTICAL) {
			
			for(int i = 0; i < 3; i++) {
				
				tabuleiro[linha + i][coluna].setId(2);
				tabuleiro[linha + i][coluna].setToolTipText("DESTROIER");
				tabuleiro[linha + i][coluna].setEstado(Estado.NAVIO);
				tabuleiro[linha + i][coluna].setIcon(imagem.destroier[i][orientacao]);
			}
		}
	}
	
	public void desenhaSubmarino(int orientacao, int linha, int coluna, Botao [][] tabuleiro) {
		
		if(orientacao == HORIZONTAL) {
			
			for(int i = 0; i < 3; i++) {
				
				tabuleiro[linha][coluna + i].setId(3);
				tabuleiro[linha][coluna + i].setToolTipText("SUBMARINO");
				tabuleiro[linha][coluna + i].setEstado(Estado.NAVIO);
				tabuleiro[linha][coluna + i].setIcon(imagem.submarino[i][orientacao]);
			}
		}
		
		if(orientacao == VERTICAL) {
			
			for(int i = 0; i < 3; i++) {
				
				tabuleiro[linha + i][coluna].setId(3);
				tabuleiro[linha + i][coluna].setToolTipText("SUBMARINO");
				tabuleiro[linha + i][coluna].setEstado(Estado.NAVIO);
				tabuleiro[linha + i][coluna].setIcon(imagem.submarino[i][orientacao]);
			}
		}
	}
	
	public void desenhaEncouracado(int orientacao, int linha, int coluna, Botao [][] tabuleiro) {
		
		if(orientacao == HORIZONTAL) {
			
			for(int i = 0; i < 4; i++) {
				
				tabuleiro[linha][coluna + i].setId(4);
				tabuleiro[linha][coluna + i].setToolTipText("ENCOURA�ADO");
				tabuleiro[linha][coluna + i].setEstado(Estado.NAVIO);
				tabuleiro[linha][coluna + i].setIcon(imagem.encouracado[i][orientacao]);
			}
		}
		
		if(orientacao == VERTICAL) {
			
			for(int i = 0; i < 4; i++) {
				
				tabuleiro[linha + i][coluna].setId(4);
				tabuleiro[linha + i][coluna].setToolTipText("ENCOURA�ADO");
				tabuleiro[linha + i][coluna].setEstado(Estado.NAVIO);
				tabuleiro[linha + i][coluna].setIcon(imagem.encouracado[i][orientacao]);
			}
		}
	}
	
	public void desenhaPortaAvioes(int orientacao, int linha, int coluna, Botao [][] tabuleiro) {
		
		if(orientacao == HORIZONTAL) {
			
			for(int i = 0; i < 5; i++) {
				
				tabuleiro[linha][coluna + i].setId(5);
				tabuleiro[linha][coluna + i].setToolTipText("PORTA_AVI�ES");
				tabuleiro[linha][coluna + i].setEstado(Estado.NAVIO);
				tabuleiro[linha][coluna + i].setIcon(imagem.portaAvioes[i][orientacao]);
			}
		}
		
		if(orientacao == VERTICAL) {
			
			for(int i = 0; i < 5; i++) {
				
				tabuleiro[linha + i][coluna].setId(5);
				tabuleiro[linha + i][coluna].setToolTipText("PORTA_AVI�ES");
				tabuleiro[linha + i][coluna].setEstado(Estado.NAVIO);
				tabuleiro[linha + i][coluna].setIcon(imagem.portaAvioes[i][orientacao]);
			}
		}
	}
}