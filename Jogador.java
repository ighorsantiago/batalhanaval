public class Jogador {

	private int num, pontuacao;
	private String nome;
	private boolean vez = false;

	public Jogador(int num, String nome, boolean vez) {

		this.num = num;
		this.nome = nome;
		this.vez = vez;
	}

	public int getNum() { return num; }
	public void setNum(int num) { this.num = num; }

	public void setNome(String nome) { this.nome = nome; }
	public String getNome() { return nome; }

	public boolean getVez() { return vez; }
	public void setVez(boolean vez) { this.vez = vez; }

	public int getPontuacao() { return pontuacao; }
	public void pontua() { pontuacao++; }
}