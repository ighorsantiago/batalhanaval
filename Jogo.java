import javax.swing.JOptionPane;

public class Jogo {

	Imagem imagem = new Imagem();
	Estado estado;
	Player player = new Player(1, "Player", false);
	Computador computador = new Computador(2, "Computador", false);
	
	int li = 0, co = 0;

	public void atiraPlayer(Botao btn) {

		if(btn.getEstado() == Estado.AGUA) {

			btn.setEstado(Estado.VAZIO);
			btn.setIcon(imagem.agua);
		}

		if(btn.getEstado() == Estado.NAVIO) {

			btn.setIcon(imagem.fogo);
			btn.setEstado(Estado.VAZIO);

			player.pontua();
		}
		
		if(player.getPontuacao() == 17) {

			JOptionPane.showMessageDialog(null,
				"Voc� ganhou!!!");

			System.exit(0);
		}
		
		atiraComputador();
	}
	
	public void atiraComputador() {

		int l = computador.sorteia(8);
		int c = computador.sorteia(8);
		
		if(player.tabuleiroPlayer[l][c].getEstado() == Estado.VAZIO) {

			player.tabuleiroPlayer[l][c].setEstado(Estado.AGUA);
			player.tabuleiroPlayer[l][c].setIcon(imagem.agua);
		}

		if(player.tabuleiroPlayer[l][c].getEstado() == Estado.NAVIO) {

			player.tabuleiroPlayer[l][c].setIcon(imagem.fogo);
			player.tabuleiroPlayer[l][c].setEstado(Estado.AFUNDADO);
			computador.pontua();
		}
		
		if(computador.getPontuacao() == 17) {

			JOptionPane.showMessageDialog(null,
				"Voc� perdeu...");

			System.exit(0);
		}
	}
}