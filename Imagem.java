import javax.swing.ImageIcon;

public class Imagem {

	ImageIcon agua = new ImageIcon(getClass().getResource("/imagens/agua.png"));
	ImageIcon fogo = new ImageIcon(getClass().getResource("/imagens/fogo.png"));
	ImageIcon mar = new ImageIcon(getClass().getResource("/imagens/mar.png"));
	ImageIcon pin = new ImageIcon(getClass().getResource("/imagens/pin.png"));

	ImageIcon bp = new ImageIcon(getClass().getResource("/imagens/barcoPatrulha.png"));

	ImageIcon bpH1 = new ImageIcon(getClass().getResource("/imagens/BarcoPatrulhaH1.png"));
	ImageIcon bpH2 = new ImageIcon(getClass().getResource("/imagens/BarcoPatrulhaH2.png"));
	ImageIcon bpV1 = new ImageIcon(getClass().getResource("/imagens/BarcoPatrulhaV1.png"));
	ImageIcon bpV2 = new ImageIcon(getClass().getResource("/imagens/BarcoPatrulhaV2.png"));

	ImageIcon [][] barcoPatrulha = {{bpH1, bpV1}, {bpH2, bpV2} };
	
	ImageIcon des = new ImageIcon(getClass().getResource("/imagens/destroier.png"));

	ImageIcon desH1 = new ImageIcon(getClass().getResource("/imagens/DestroierH1.png"));
	ImageIcon desH2 = new ImageIcon(getClass().getResource("/imagens/DestroierH2.png"));
	ImageIcon desH3 = new ImageIcon(getClass().getResource("/imagens/DestroierH3.png"));
	ImageIcon desV1 = new ImageIcon(getClass().getResource("/imagens/DestroierV1.png"));
	ImageIcon desV2 = new ImageIcon(getClass().getResource("/imagens/DestroierV2.png"));
	ImageIcon desV3 = new ImageIcon(getClass().getResource("/imagens/DestroierV3.png"));

	ImageIcon [][] destroier = {{desH1, desV1}, {desH2, desV2}, {desH3, desV3} };
	
	ImageIcon sub = new ImageIcon(getClass().getResource("/imagens/submarino.png"));

	ImageIcon subH1 = new ImageIcon(getClass().getResource("/imagens/SubmarinoH1.png"));
	ImageIcon subH2 = new ImageIcon(getClass().getResource("/imagens/SubmarinoH2.png"));
	ImageIcon subH3 = new ImageIcon(getClass().getResource("/imagens/SubmarinoH3.png"));
	ImageIcon subV1 = new ImageIcon(getClass().getResource("/imagens/SubmarinoV1.png"));
	ImageIcon subV2 = new ImageIcon(getClass().getResource("/imagens/SubmarinoV2.png"));
	ImageIcon subV3 = new ImageIcon(getClass().getResource("/imagens/SubmarinoV3.png"));

	ImageIcon [][] submarino = {{subH1, subV1}, {subH2, subV2}, {subH3, subV3} };

	ImageIcon enc = new ImageIcon(getClass().getResource("/imagens/encouracado.png"));

	ImageIcon encH1 = new ImageIcon(getClass().getResource("/imagens/EncouracadoH1.png"));
	ImageIcon encH2 = new ImageIcon(getClass().getResource("/imagens/EncouracadoH2.png"));
	ImageIcon encH3 = new ImageIcon(getClass().getResource("/imagens/EncouracadoH3.png"));
	ImageIcon encH4 = new ImageIcon(getClass().getResource("/imagens/EncouracadoH4.png"));
	ImageIcon encV1 = new ImageIcon(getClass().getResource("/imagens/EncouracadoV1.png"));
	ImageIcon encV2 = new ImageIcon(getClass().getResource("/imagens/EncouracadoV2.png"));
	ImageIcon encV3 = new ImageIcon(getClass().getResource("/imagens/EncouracadoV3.png"));
	ImageIcon encV4 = new ImageIcon(getClass().getResource("/imagens/EncouracadoV4.png"));

	ImageIcon [][] encouracado = {{encH1, encV1}, {encH2, encV2}, {encH3, encV3}, {encH4, encV4} };
	
	ImageIcon pa = new ImageIcon(getClass().getResource("/imagens/portaAvioes.png"));

	ImageIcon paH1 = new ImageIcon(getClass().getResource("/imagens/PortaAvioesH1.png"));
	ImageIcon paH2 = new ImageIcon(getClass().getResource("/imagens/PortaAvioesH2.png"));
	ImageIcon paH3 = new ImageIcon(getClass().getResource("/imagens/PortaAvioesH3.png"));
	ImageIcon paH4 = new ImageIcon(getClass().getResource("/imagens/PortaAvioesH4.png"));
	ImageIcon paH5 = new ImageIcon(getClass().getResource("/imagens/PortaAvioesH5.png"));
	ImageIcon paV1 = new ImageIcon(getClass().getResource("/imagens/PortaAvioesV1.png"));
	ImageIcon paV2 = new ImageIcon(getClass().getResource("/imagens/PortaAvioesV2.png"));
	ImageIcon paV3 = new ImageIcon(getClass().getResource("/imagens/PortaAvioesV3.png"));
	ImageIcon paV4 = new ImageIcon(getClass().getResource("/imagens/PortaAvioesV4.png"));
	ImageIcon paV5 = new ImageIcon(getClass().getResource("/imagens/PortaAvioesV5.png"));
	
	ImageIcon [][] portaAvioes = {{paH1, paV1}, {paH2, paV2}, {paH3, paV3}, {paH4, paV4}, {paH5, paV5} };
}