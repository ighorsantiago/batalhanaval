import java.util.Random;

public class Computador extends Jogador {

	Botao btn;
	TelaPrincipal tabuleiro;
	Navio navio = new Navio();
	
	int orientacao, linha, coluna;
	
	Random r = new Random();
	
	public static final int HORIZONTAL = 0;
	public static final int VERTICAL = 1;

	Botao [][] tabuleiroComputador = new Botao [10][10];
	
	public Computador(int num, String nome, boolean vez) {
		
		super(2, "Computador", false);
	}
	
	public int sorteia(int x) { return r.nextInt(x); }
	
	public void posicionaComputador() {
		
		int navios = 1;
		
		while(navios <= 5) {
		
			orientacao = sorteia(2);
			linha = sorteia(10);
			coluna = sorteia(10);
			
			navio = navio.constroiNavio(navios);
			
			if(navio.getTamanho() != 0) {

				if(orientacao < 2) {
					
					if(linha < 10) {
					
						if(coluna < 10) {
							
							if(navio.posicionaNavio("C", orientacao, linha, coluna, navio, tabuleiroComputador)) {

								navios++;
							}
						}
						else
							posicionaComputador();
					}
					else
						posicionaComputador();
				}
				else
					posicionaComputador();
			}
			else
				posicionaComputador();
		}
	}
}