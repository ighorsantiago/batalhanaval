import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JSeparator;
import javax.swing.border.TitledBorder;

public class TelaPrincipal implements ActionListener {

	Botao btn = new Botao();
	Imagem imagem;
	Jogo jogo = new Jogo();

	JPanel telaPrincipal, telaPlayer, telaComputador;
	
	ActionEvent evento;
	JMenuItem novo, sobre, sair;
	
	private JFrame frame;
	
	int n = 1;
	
	Player player = new Player(1, "Player", false);
	Computador computador = new Computador(2, "Computador", false);

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					TelaPrincipal window = new TelaPrincipal();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	public void imprime(Botao [][] tab) {
		
		System.out.println(tab[0][0].getNome());
		
		for(int i = 0; i < 10; i++) {
			
			for(int g = 0; g < 10; g++) {
				
				int l = tab[i][g].getLinha();
				int c = tab[i][g].getColuna();
				
				System.out.print("[" + l + c + "]");
			}
			
			System.out.print("	");
			
			for(int h = 0; h < 10; h++) {
				
				System.out.print("[" + tab[i][h].getEstado() + "]");
			}
			
			System.out.println("");
		}
	}
	
	public void criaTabuleiro(JPanel tela, Botao [][] tabuleiro, String jogador) {
		
		for(int i = 0; i < 10; i++) {

			for(int g = 0; g < 10; g++) {

				tabuleiro[i][g] = new Botao();
				tabuleiro[i][g].setName(jogador + " " + String.valueOf(i * 10 + g));
				tabuleiro[i][g].setEstado(Estado.VAZIO);
				tabuleiro[i][g].setId(0);
				tabuleiro[i][g].setLinha(i);
				tabuleiro[i][g].setColuna(g);
				tabuleiro[i][g].setToolTipText("ID: " + String.valueOf(tabuleiro[i][g].getId()));
				tabuleiro[i][g].addMouseListener(new MouseAdapter() {
					
					@Override
					public void mouseClicked(MouseEvent e) {
						
						Botao btn = (Botao) e.getSource();
						if(player.navios <= 5) { player.posicionaPlayer(btn); }
						else jogo.atiraPlayer(btn);
					}
				});
				
				tela.add(tabuleiro[i][g]);
			}
		}

		if(jogador.equals("Player")) {

			player.setNome(JOptionPane.showInputDialog(null, "Digite seu nome:"));
			tela.setBorder(BorderFactory.createTitledBorder("Alm. " + player.getNome()));
		}
		else
			tela.setBorder(BorderFactory.createTitledBorder("Alm. Computador"));
	}

	/**
	 * Create the application.
	 */
	public TelaPrincipal() {
		initialize();
	}
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		frame = new JFrame();
		frame.setBounds(100, 100, 750, 435);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		telaPrincipal = new JPanel();
		frame.getContentPane().add(telaPrincipal, BorderLayout.CENTER);
		telaPrincipal.setLayout(new GridLayout(0, 2));
		
		telaPlayer = new JPanel();
		
		telaPlayer.setBackground(Color.WHITE);
		telaPlayer.setBorder(new TitledBorder(null, "Player", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		telaPrincipal.add(telaPlayer);
		
		criaTabuleiro(telaPlayer, player.tabuleiroPlayer, "Player");

imprime(player.tabuleiroPlayer);
		
		telaComputador = new JPanel();
		telaComputador.setBorder(new TitledBorder(null, "Computador", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		telaPrincipal.add(telaComputador);
		
		criaTabuleiro(telaComputador, computador.tabuleiroComputador, "Computador");

imprime(computador.tabuleiroComputador);

		JMenuBar menu = new JMenuBar();
		frame.setJMenuBar(menu);
		
		JMenu jogo = new JMenu("Jogo");
		menu.add(jogo);
		
		novo = new JMenuItem("Novo Jogo");
		novo.addActionListener(this);
		jogo.add(novo);
		
		JSeparator separator = new JSeparator();
		jogo.add(separator);
		
		sobre = new JMenuItem("Sobre");
		sobre.addActionListener(this);
		jogo.add(sobre);
		
		JSeparator separator_1 = new JSeparator();
		jogo.add(separator_1);
		
		sair = new JMenuItem("Sair");
		sair.addActionListener(this);
		jogo.add(sair);
	}
	
	public void actionPerformed(ActionEvent evento) {

		this.evento = evento;

		if(evento.getSource() == novo) {
			
			computador.posicionaComputador();
		}

		if(evento.getSource() == sobre) {

			JOptionPane.showMessageDialog(null, "Autor: Ighor Santiago\n"
				+ "\nBatalha Naval\n"
				+ "\nPosicione seus navios enquanto seu adversário posiciona os dele."
				+ "\nAcerte os navios adversários antes que ele acerte os seus.\n"
				+ "\nBOA SORTE!");
		}
		
		if(evento.getSource() == sair) {

			System.exit(0);
		}
	}
}