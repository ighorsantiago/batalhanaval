import javax.swing.*;

import java.awt.*;

public class Botao extends JLabel {

	Imagem imagem = new Imagem();
	
	Estado estado;
	int id, linha, coluna;
	String nome;
	
	Botao selecionado;
	Ponto posicao;
	
	public Botao() {
		
		super();
		
		init();
	}

	public String getNome() { return nome; }
	public void setNome(String nome) { this.nome = nome; }
	
	public Estado getEstado() { return estado; }
	public void setEstado(Estado estado) { this.estado = estado; }

	public int getId() { return id; }
	public void setId(int id) { this.id = id; }
	
	public Botao getSelecionado() { return selecionado; }
	public void setSelecionado(Botao selecionado) { this.selecionado = selecionado; }
	
	public int getLinha() { return linha; }
	public void setLinha(int linha) { this.linha = linha; }	

	public int getColuna() { return coluna; }
	public void setColuna(int coluna) { this.coluna = coluna; }

	public void init() {
		
		setText("");
		setOpaque(true);
		setBorder(BorderFactory.createLineBorder(Color.BLACK));
		setPreferredSize(new Dimension(30, 30));
		setHorizontalAlignment(SwingConstants.CENTER);
		setBackground(Color.LIGHT_GRAY);
		setIcon(imagem.mar);
		estado = Estado.AGUA;
	}
}